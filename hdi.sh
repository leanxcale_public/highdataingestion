#!/bin/bash

MAIN=$(ls -t highdataingestion-* | head -1)
DRIVER=$(ls -t lxjdbcdriver* 2> /dev/null | head -1)
MAINCLASS=com.leanxcale.highdataingestion.Main

PROPS=${1:-"config.properties"}

if [[ "$PROPS" != */* ]]; then
  PROPS="$PWD/$PROPS"
fi

START=$(date +%T)
DURATION=$SECONDS

java -cp $MAIN:$DRIVER -DMAIN_PROP="$PROPS" $MAINCLASS
#2> /dev/null

END=$(date +%T)
DURATION=$((SECONDS-DURATION))

# DURATION
printf "START\t\tEND\t\tDURATION\n"
printf "%s\t%s\t%d\n\n" $START $END $DURATION

