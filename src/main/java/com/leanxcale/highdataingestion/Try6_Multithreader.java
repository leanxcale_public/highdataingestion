package com.leanxcale.highdataingestion;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.leanxcale.highdataingestion.reader.Loan;
import com.leanxcale.highdataingestion.reader.LoansReader;
import lombok.extern.slf4j.Slf4j;


/**
 * This test commits the data in batches in n-threads
 */

@Slf4j
public class Try6_Multithreader extends Try_abstract {
	private final static int BATCH_SIZE = 1000;
	private final static int N_THREADS = 20;


	public void createTable() throws SQLException {

		String SQL_CREATE_TABLE_HASH =
				"CREATE TABLE " + TABLE_NAME + " (" +
						COLS +
						"PRIMARY KEY (client_id,loan_id)" +
						")" +
						" PARTITION BY HASH (client_id)" +
						" DISTRIBUTE BY HASH";

		utils.createTable(SQL_CREATE_TABLE_HASH);
	}

// tag::ingest[]
	public void ingestData() {
		try (LoansReader csv = new LoansReader())
		{
			List<Callable<Object>> task = new ArrayList<>(N_THREADS);
			for (int i = 0; i < N_THREADS; i++) {
				task.add(Executors.callable(new doIngestData(csv, i)));
			}
			ExecutorService taskExecutor = Executors.newFixedThreadPool(N_THREADS);
			taskExecutor.invokeAll(task);
			taskExecutor.shutdown();
		} catch (InterruptedException e) {
			log.trace("Threaded dataIngest interrupted", e);
		}
	}
// end::ingest[]

// tag::thread[]
	private class doIngestData implements Runnable {
		private final int indx;
		private final LoansReader csv;

		doIngestData(LoansReader csv, int indx)
		{
			this.csv = csv;
			this.indx = indx;
		}
// end::thread[]

		@Override
// tag::run[]
		public void run() {

			try (LoansReader.Section section = csv.createSection(indx, N_THREADS);
				 Connection connection = utils.connect();
				 PreparedStatement prepStmt = connection.prepareStatement(utils.SQL_INSERT_PREPARED))
			{
				//...
// end::run[]
				int batchCount = 0;
				Loan loan;
				while ((loan = csv.read(section)) != null) {
					setParams(prepStmt, loan);
					prepStmt.addBatch();
					if (++batchCount == BATCH_SIZE) {
						prepStmt.executeBatch();
						connection.commit();
						batchCount = 0;
						stats.send(BATCH_SIZE);
					}
				}
				if (batchCount != 0) {
					prepStmt.executeBatch();
					connection.commit();
					stats.send(batchCount);
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	}

	private void setParams(PreparedStatement prepStmt, Loan loan) throws SQLException {
		Object[] args = utils.getParams(loan);
		for (int j = 0; j < args.length; j++) {
			prepStmt.setObject(j + 1, args[j]);
		}
	}
}
