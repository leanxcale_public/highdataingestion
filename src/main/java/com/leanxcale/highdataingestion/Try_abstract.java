package com.leanxcale.highdataingestion;

import com.leanxcale.highdataingestion.stats.StatsPrinter;
import lombok.extern.slf4j.Slf4j;

import java.sql.SQLException;

/**
 * This test inserts full sql sentences opening the connection each insert and committing for each row.
 */

@Slf4j
public abstract class Try_abstract implements Try_DB {

	protected static final String TABLE_NAME = "loans";
	protected static final String COLS =
					 "client_id bigint,"
					+ "loan_id bigint,"
					+ "ts timestamp,"
					+ "issue_d bigint,"
					+ "loan_status int," // 0 rejected, 1 accepted-paid, 2 accepted-defaulted, 3 accepted-other?
					+ "loan_amnt double,"
					+ "term int,"
					+ "int_rate double,"
					+ "installment double,"
					+ "grade varchar,"
					+ "emp_length double,"
					+ "home_ownership varchar,"
					+ "annual_inc double,"
					+ "verification_status varchar,"
					+ "mths_since_issue_d int,"
					+ "\"DESC\" varchar,"
					+ "purpose varchar,"
					+ "title varchar,"
					+ "zip_code varchar,"
					+ "addr_state varchar,"
					+ "dti double,"
					+ "mths_since_earliest_cr_line int,"
					+ "inq_last_6mths double,"
					+ "revol_util double,"
					+ "out_prncp double,"
					+ "total_pymnt double,"
					+ "total_rec_int double,"
					+ "last_pymnt_d varchar,"
					+ "mths_since_last_pymnt_d int,"
					+ "mths_since_last_credit_pull_d int,"
					+ "total_rev_hi_lim double,";


	protected final DBUtils utils;
	protected StatsPrinter stats;

	public Try_abstract() {
		String name = getClass().getSimpleName();
		log.info("[TRY] Running " + name + "...\n");
		utils = new DBUtils(TABLE_NAME, COLS);
	}

	public void dropTable() throws SQLException {
		utils.cleanDatabase();
	}

	public void run() throws SQLException {
		createTable();
		try (StatsPrinter stats = new StatsPrinter()) {
			this.stats = stats;
			ingestData();
		}
		dropTable();
	}
}
