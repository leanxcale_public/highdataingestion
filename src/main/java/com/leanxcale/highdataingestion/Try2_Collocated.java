package com.leanxcale.highdataingestion;


import lombok.extern.slf4j.Slf4j;


/**
 * This test is exactly the same code as Test1_NaiveApproach but should be run collocated to the LX server instead of from your laptop
 */

@Slf4j
public class Try2_Collocated extends Try1_NaiveApproach { }

