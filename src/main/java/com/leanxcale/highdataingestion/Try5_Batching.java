package com.leanxcale.highdataingestion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.leanxcale.highdataingestion.reader.Loan;
import com.leanxcale.highdataingestion.reader.LoansReader;
import lombok.extern.slf4j.Slf4j;


/**
 * This test commits the data in batches
 */

@Slf4j
public class Try5_Batching extends Try_abstract {

	private final static int BATCH_SIZE = 1000;

	public void createTable() throws SQLException {

		String SQL_CREATE_TABLE_HASH =
				"CREATE TABLE " + TABLE_NAME + " (" +
						COLS +
						"PRIMARY KEY (client_id,loan_id)" +
						")" +
						" PARTITION BY HASH (client_id)" +
						" DISTRIBUTE BY HASH";

		utils.createTable(SQL_CREATE_TABLE_HASH);
	}

	public void ingestData() {
		try (LoansReader csv = new LoansReader();
			 Connection connection = utils.connect();
			 PreparedStatement prepStmt = connection.prepareStatement(utils.SQL_INSERT_PREPARED))
		{
// tag::ingest[]
			int batchCount = 0;
			Loan loan;
			while ((loan = csv.read()) != null) {
				setParams(prepStmt, loan);
				prepStmt.addBatch();
				if (++batchCount == BATCH_SIZE) {
					prepStmt.executeBatch();
					connection.commit();
					batchCount = 0;
					stats.send(BATCH_SIZE);
				}
			}
			if (batchCount != 0) {
				prepStmt.executeBatch();
				connection.commit();
				stats.send(batchCount);
			}
// end::ingest[]
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	private void setParams(PreparedStatement prepStmt, Loan loan) throws SQLException {
		Object[] args = utils.getParams(loan);
		for (int j = 0; j < args.length; j++) {
			prepStmt.setObject(j + 1, args[j]);
		}
	}
}
