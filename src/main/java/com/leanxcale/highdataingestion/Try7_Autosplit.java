package com.leanxcale.highdataingestion;

import java.sql.SQLException;

import lombok.extern.slf4j.Slf4j;


/**
 * This test adds LX autosplit partitioning.
 */

@Slf4j
public class Try7_Autosplit extends Try6_Multithreader {

	@Override
// tag::create[]
	public void createTable() throws SQLException {

		String SQL_CREATE_TABLE_HASH_AUTOSPLIT =
				"CREATE TABLE " + TABLE_NAME + " (" +
						COLS +
						"PRIMARY KEY (client_id,loan_id,ts)" +	//bidimensional field required as part of PK
						")" +
						" PARTITION BY HASH (client_id)" +
						" PARTITION BY DIMENSION ts EVERY INTERVAL '30' SECONDS KEEP INTERVAL '5' MINUTES" +
						" DISTRIBUTE BY HASH";

		utils.createTable(SQL_CREATE_TABLE_HASH_AUTOSPLIT);
	}
// end::create[]
}


