package com.leanxcale.highdataingestion;

import java.sql.SQLException;

import com.leanxcale.highdataingestion.reader.LoansReader;
import lombok.extern.slf4j.Slf4j;


/**
 * This test uses a bidimensional partitioning with a uniform distribution of data based on the PK
 */

@Slf4j
public class Try9_PrimaryKeyDistribution extends Try6_Multithreader {

	private static final int REGIONS = 8;

	@Override
	public void createTable() throws SQLException {
		String splitPoints;
		try (LoansReader loansReader = new LoansReader())
		{
			splitPoints = utils.getSplitPoints(loansReader, REGIONS);
		}
		log.info("Split Points:{}", splitPoints);

		String SQL_CREATE_TABLE_PKRANGE_AUTOSPLIT =
				"CREATE TABLE " + TABLE_NAME + " (" +
				COLS +
				"PRIMARY KEY (client_id,loan_id,ts)" +	//bidimensional field required as part of PK
				")" +
				" PARTITION BY KEY (client_id,loan_id) AT " + splitPoints +
				" PARTITION BY DIMENSION ts EVERY INTERVAL '30' SECONDS KEEP INTERVAL '5' MINUTES" +
				" DISTRIBUTE BY KEY";

		utils.createTable(SQL_CREATE_TABLE_PKRANGE_AUTOSPLIT);
	}
}
