package com.leanxcale.highdataingestion;

import java.sql.SQLException;

public interface Try_DB {
	void createTable() throws SQLException;
	void dropTable() throws SQLException;

	void ingestData();
	void run() throws SQLException, InterruptedException;
}
