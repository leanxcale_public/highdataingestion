package com.leanxcale.highdataingestion.avl;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AVLTree<T extends Comparable<T>> {

	public class Node {
		T key;
		int height;
		Node left;
		Node right;

		Node(T key) {
			this.key = key;
		}

	}

	private Node root;
	private long size;

	public Node find(T key) {
		Node current = root;
		while (current != null) {
			if (current.key == key) {
				break;
			}
			current = current.key.compareTo(key) < 0 ? current.right : current.left;
		}
		return current;
	}

	public synchronized void insert(T key) {
		root = insert(root, key);
	}

	public synchronized void delete(T key) {
		root = delete(root, key);
	}

	public Node getRoot() {
		return root;
	}

	public int height() {
		return root == null ? -1 : root.height;
	}

	public List<T> getSplitKeys(int splits) {
		if (splits < 2) {
			log.info("2 or more splits are required");
			return null;
		}

		int sizeSplit = (int) (this.size / splits);
		List<Long> splitPositions = new ArrayList<>(splits - 1);
		for (long i = 1; i <= splits - 1; i++) {
			splitPositions.add(i * sizeSplit);
		}

		ArrayList<T> keys = new ArrayList<>(splitPositions.size());
		getSplitKeys(keys, splitPositions, this.root, new AtomicInteger(0));

		return keys;
	}

	private void getSplitKeys(ArrayList<T> keys, List<Long> splitPositions, Node node, AtomicInteger c) {
		//go to the deeper left node
		if (node.left != null) {
			getSplitKeys(keys, splitPositions, node.left, c);
		}

		if (keys.size() == splitPositions.size()) return; //already found all splits

		if (c.getAndIncrement() == splitPositions.get(keys.size())) { //this node correspond to the next split point
			keys.add(node.key);
			if (keys.size() == splitPositions.size()) return; //end. all split positions got
		}

		if (node.right != null) {
			getSplitKeys(keys, splitPositions, node.right, c);
		}

	}


	public void printSort() {
		StringBuffer buf = new StringBuffer();
		printSort(this.root, buf);
		log.info("AVLTree: {}", buf.toString());
	}

	private void printSort(Node node, StringBuffer buf) {
		if (node.left != null) {
			printSort(node.left, buf);
		}
		buf.append(node.key.toString()).append(", ");
		if (node.right != null) {
			printSort(node.right, buf);
		}
	}

	private Node insert(Node node, T key) {
		if (node == null) {
			this.size++;
			return new Node(key);
		} else if (node.key.compareTo(key) > 0) {
			node.left = insert(node.left, key);
		} else if (node.key.compareTo(key) < 0) {
			node.right = insert(node.right, key);
		} else {
			// duplicate key. Ignore, don't insert
			return node;
		}
		return rebalance(node);
	}

	private Node delete(Node node, T key) {
		if (node == null) {
			return node;
		} else if (node.key.compareTo(key) > 0) {
			node.left = delete(node.left, key);
		} else if (node.key.compareTo(key) < 0) {
			node.right = delete(node.right, key);
		} else {
			if (node.left == null || node.right == null) {
				node = (node.left == null) ? node.right : node.left;
			} else {
				Node mostLeftChild = mostLeftChild(node.right);
				node.key = mostLeftChild.key;
				node.right = delete(node.right, node.key);
			}
		}
		if (node != null) {
			node = rebalance(node);
		}
		return node;
	}

	private Node mostLeftChild(Node node) {
		Node current = node;
		/* loop down to find the leftmost leaf */
		while (current.left != null) {
			current = current.left;
		}
		return current;
	}

	private Node rebalance(Node z) {
		updateHeight(z);
		int balance = getBalance(z);
		if (balance > 1) {
			if (height(z.right.right) > height(z.right.left)) {
				z = rotateLeft(z);
			} else {
				z.right = rotateRight(z.right);
				z = rotateLeft(z);
			}
		} else if (balance < -1) {
			if (height(z.left.left) > height(z.left.right)) {
				z = rotateRight(z);
			} else {
				z.left = rotateLeft(z.left);
				z = rotateRight(z);
			}
		}
		return z;
	}

	private Node rotateRight(Node y) {
		Node x = y.left;
		Node z = x.right;
		x.right = y;
		y.left = z;
		updateHeight(y);
		updateHeight(x);
		return x;
	}

	private Node rotateLeft(Node y) {
		Node x = y.right;
		Node z = x.left;
		x.left = y;
		y.right = z;
		updateHeight(y);
		updateHeight(x);
		return x;
	}

	private void updateHeight(Node n) {
		n.height = 1 + Math.max(height(n.left), height(n.right));
	}

	private int height(Node n) {
		return n == null ? -1 : n.height;
	}

	public int getBalance(Node n) {
		return (n == null) ? 0 : height(n.right) - height(n.left);
	}
}
