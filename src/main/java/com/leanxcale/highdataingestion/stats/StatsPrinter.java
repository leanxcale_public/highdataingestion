package com.leanxcale.highdataingestion.stats;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;


import lombok.extern.slf4j.Slf4j;

@Slf4j
public class StatsPrinter implements AutoCloseable {
	private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");

	private final AtomicLong startTs = new AtomicLong(0);

	private final AtomicLong added = new AtomicLong(0);
	private long prevAdded = 0;
	private PrintWriter printWriter;
	private final static boolean printCsvStats = false;
	private final static int FREQUENCY = 5;

	ScheduledExecutorService taskExecutor;

	public StatsPrinter() {
		this(true);
	}

	// Non thread-safe stats init. Intended for init before threads operations
	public StatsPrinter(boolean startNow) {

		if (printCsvStats) {
			try {
				printWriter = new PrintWriter(new FileWriter("UFDI_stats_" + df.format(new Date()) + ".csv"));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			printWriter.println("ts,inserts,inserts/sec");
		}

		taskExecutor = Executors.newSingleThreadScheduledExecutor();
		if (startNow) {
			start();
		}
	}

	// Thread-safe stats start
	public void start() {
		if (startTs.compareAndSet(0, System.currentTimeMillis())) {
			log.info("[STATS] Running stats printer from ts:{}", startTs.get());
			Runnable statsTask = () -> {
				long curAdded = added.get();
				long count = curAdded - prevAdded;
				prevAdded = curAdded;
				String insertsPerSecond = String.format("%.1f", (float) count / FREQUENCY);
				log.info("[STATS] Added [{}] in the last [{}] seconds. Insert rate:[{}/sec].", count, FREQUENCY, insertsPerSecond);
				if (printCsvStats) {
					printWriter.println(System.currentTimeMillis() + "," + count + "," + insertsPerSecond);
				}
			};
			taskExecutor.scheduleAtFixedRate(statsTask, FREQUENCY, FREQUENCY, TimeUnit.SECONDS);
		}
	}

	// Non thread-safe stats closing. Intended for close after threads operations
	@Override
	public void close() {
		taskExecutor.shutdown();
		try {
			if (!taskExecutor.awaitTermination(FREQUENCY, TimeUnit.SECONDS)) {
				taskExecutor.shutdownNow();
			}
		} catch (InterruptedException e) {
			// throw new RuntimeException(e);
		}
		long endTs = System.currentTimeMillis();

		if (printWriter != null) {
			printWriter.close();
		}
		float secs = (endTs - startTs.get()) / 1_000f;
		long count = added.get();
		String insertsPerSecond = String.format("%.1f", count / secs);
		String elapsed = String.format("%.1f", secs);
		log.info("[STATS] TOTAL Added [{}] in [{}] seconds. Insert rate:[{}/sec].", count, elapsed, insertsPerSecond);
		log.info("[STATS] Stopped at ts:{}", endTs);
	}

	// Thread-safe stats accounting
	public void send(int n) {
		added.addAndGet(n);
	}

}
