package com.leanxcale.highdataingestion;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.leanxcale.highdataingestion.reader.Loan;
import com.leanxcale.highdataingestion.reader.LoansReader;
import lombok.extern.slf4j.Slf4j;

/**
 * This test inserts full sql sentences opening just one connection that remains open and commits for each row.
 */

@Slf4j
public class Try3_PermanentConnection extends Try_abstract {

	public void createTable() throws SQLException {

		String SQL_CREATE_TABLE_HASH =
				"CREATE TABLE " + TABLE_NAME + " (" +
						COLS +
						"PRIMARY KEY (client_id,loan_id)" +
						")" +
						" PARTITION BY HASH (client_id)" +
						" DISTRIBUTE BY HASH";

		utils.createTable(SQL_CREATE_TABLE_HASH);
	}

   // tag::ingest[]
 	public void ingestData() {
		try (LoansReader csv = new LoansReader();
			 Connection connection = utils.connect();
			 Statement stmt = connection.createStatement())
		{
			Loan loan;
			while ((loan = csv.read()) != null) {
				String sql = utils.getInsertSql(loan);
 // tag::execute[]
                stmt.executeUpdate(sql); //insert the row
 // end::execute[]
				connection.commit();
				stats.send(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
// end::ingest[]
}
