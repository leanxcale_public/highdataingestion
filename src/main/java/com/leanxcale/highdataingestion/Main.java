package com.leanxcale.highdataingestion;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Main {

	public static void main(String[] args) {
		new Main().run();
	}

	public void run() {
		try {
			new Try1_NaiveApproach().run();
			new Try2_Collocated().run();
			new Try3_PermanentConnection().run();
			new Try4_PreparedStatements().run();
			new Try5_Batching().run();
			new Try6_Multithreader().run();
			new Try7_Autosplit().run();
			new Try9_PrimaryKeyDistribution().run();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
