package com.leanxcale.highdataingestion.reader;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;


import lombok.extern.slf4j.Slf4j;


@Slf4j
public class LoansReader implements AutoCloseable {

	protected final Date NOW_DATE = new Date();

	private final AtomicLong ID;
	private long endTime;

	private final static String SEPARATOR = ",";
	private final static String FILENAME = "loans.csv";
	private final File loansFile;

	public class Section implements AutoCloseable {
		RandomAccessFile raf;
		long beginPos;
		long endPos;

		Section() throws IOException {
			raf = new RandomAccessFile(loansFile, "r");
			raf.readLine();		// Skip headers
			beginPos = raf.getFilePointer();
			endPos = raf.length();
		}

		@Override
		public void close() {
			try {
				raf.close();
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
	}
	Section defaultSection;

	//public LoansReader(int section, int totalSections) {
	public LoansReader() {
		ID = new AtomicLong(0); //starts on this client id and will be increased by totalClients each row to avoid PK collision

		loansFile = copyFromResource();
		defaultSection = createSection(0, 1);

		// 30 seconds
		setDuration(30);
	}

	File copyFromResource() {
		try (InputStream is = getClass().getClassLoader().getResourceAsStream(LoansReader.FILENAME)) {
			File tmpFile = File.createTempFile(LoansReader.FILENAME, "tmp");
			Files.copy(Objects.requireNonNull(is), tmpFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
			return tmpFile;
		} catch (Exception e) {
			throw new RuntimeException("Error reading csv:" + LoansReader.FILENAME, e);
		}
	}

	// Bound reading to this section of file
	// indx is in [0, numSections - 1]
	public Section createSection(int indx, int numSections) {
		log.debug("Read Section [{}/{}]", indx, numSections);
		try {
			Section section = new Section();
			//divide the file in n equitable parts
			long sectionSize = (section.endPos - section.beginPos) / numSections;
			section.endPos = section.beginPos + (indx + 1) * sectionSize;
			if (indx > 0) {
				// Skip first line of section (will be read by the previous section)
				section.raf.seek(section.endPos - sectionSize);    //go to the start of the section.
				section.raf.readLine();        //discard this partial line (will be read by the previous section thread)
				section.beginPos = section.raf.getFilePointer();
			}
			return section;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void setDuration(long duration) {
		if (duration <= 0) {
			log.info("Duration = read 'till eof");
			endTime = 0;
		} else {
			endTime = System.currentTimeMillis() + duration * 1000;
			log.info("Duration = " + duration);
		}
	}

	@Override
	public void close() {
		defaultSection.close();
	}

	public Loan read(Section section) {
		// Check duration
		if (endTime > 0 && System.currentTimeMillis() > endTime) {
			return null;
		}
		String line = null;
		try {
			if (section.raf.getFilePointer() <= section.endPos) { //pointer is still inside its section
				line = section.raf.readLine();
			}
			// check end of section/file and rewind if endTime > 0
			if (line == null) {
				if (endTime <= 0)  {
					return null;
				}
				section.raf.seek(section.beginPos);	 //go to the start of the section.
				line = section.raf.readLine();
			}
			return parseLoan(Objects.requireNonNull(line).split(SEPARATOR));
		} catch (ParseException e) {
			log.error("Error reading parsing loan:{}", line, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			throw new RuntimeException("Error reading csv:", e);
		}
	}

	public Loan read() {
		return read(defaultSection);
	}

	/* convert a csv line in a loan object */
	private Loan parseLoan(String[] values) throws ParseException {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		DateFormat formatter = new SimpleDateFormat("MMM-yyyy", Locale.US);

		Loan loan = new Loan();
		loan.setId(ID.addAndGet(1)); //for testing/educational purposes. autogenerate an incremental id for the loan
		loan.setTs(System.currentTimeMillis()); // for testing/educational purposes. Set the loan ts to current data

		String loanStatus = values[12];
		int loan_status = 0; // Rejected
		if (!"rejected".equals(loanStatus)) {
			// Accepted
			if (loanStatus.contains("Fully Paid")) {
				loan_status = 1;
			} else if (loanStatus.contains("Charged Off")) {
				loan_status = 2;
			} else {
				loan_status = 3;
			}
		}

		loan.setLoan_status(loan_status);

		loan.setClientId(readInt(values[1]));
		loan.setLoan_amnt(readDouble(values[2]));
		loan.setTerm(readInt(values[3].replaceAll(" months", "").trim()));
		loan.setInt_rate(readDouble(values[4]));
		loan.setInstallment(readDouble(values[5]));
		loan.setGrade(values[6]);
		loan.setEmp_length(readDouble(values[7].replaceAll("10\\+", "10").replaceAll("< 1", "0").replaceAll(" years", "").replaceAll(" year", "")));
		loan.setHome_ownership(values[8]);
		loan.setAnnual_inc(readDouble(values[9]));
		loan.setVerification_status(values[10]);
		int monthsSince_issue_d = getMonthsSince(values[11], calendar, formatter);
		long ts = getMonthTs(values[11], calendar, formatter);
		loan.setIssue_d(ts);
		loan.setMths_since_issue_d(monthsSince_issue_d);
		loan.setDesc(values[13].replaceAll("'", "''"));
		loan.setPurpose(values[14]);
		loan.setTitle(values[15].replaceAll("'", "''"));
		loan.setZip_code(values[16]);
		loan.setAddr_state(values[17]);
		loan.setDti(readDouble(values[18]));
		int mths_since_earliest_cr_line = getMonthsSince(values[19], calendar, formatter);
		loan.setMths_since_earliest_cr_line(mths_since_earliest_cr_line);
		loan.setInq_last_6mths(readDouble(values[20]));
		loan.setRevol_util(readDouble(values[21]));
		loan.setOut_prncp(readDouble(values[22]));
		loan.setTotal_pymnt(readDouble(values[23]));
		loan.setTotal_rec_int(readDouble(values[24]));
		int mths_since_last_pymnt_d = getMonthsSince(values[25], calendar, formatter);
		loan.setLast_pymnt_d(values[25]);
		loan.setMths_since_last_pymnt_d(mths_since_last_pymnt_d);
		int mths_since_last_credit_pull_d = (values.length > 26) ? getMonthsSince(values[26], calendar, formatter) : 0; //last empty fields are not included in the values by the split(,)
		loan.setMths_since_last_credit_pull_d(mths_since_last_credit_pull_d);
		double total_rev_hi_lim = (values.length == 28) ? readDouble(values[27]) : 0;
		loan.setTotal_rev_hi_lim(total_rev_hi_lim);
		return loan;
	}

	protected int readInt(String value) {
		return "".equals(value) ? 0 : Integer.parseInt(value);
	}

	protected double readDouble(String value) {
		return "".equals(value) ? 0 : Double.parseDouble(value);
	}

	protected int getMonthsSince(String value, Calendar calendar, DateFormat formatter) throws ParseException {
		if (value == null || value.isEmpty()) {
			return 0;
		}
		calendar.setTime(formatter.parse(value));
		int nMonthFrom = 12 * calendar.get(Calendar.YEAR) + calendar.get(Calendar.MONTH);
		calendar.setTime(NOW_DATE);
		int nMonthNow = 12 * calendar.get(Calendar.YEAR) + calendar.get(Calendar.MONTH);
		return nMonthNow - nMonthFrom;
	}

	protected long getMonthTs(String d, Calendar calendar, DateFormat formatter) throws ParseException {
		Date date = formatter.parse(d);
		calendar.setTime(date);
		return calendar.getTimeInMillis();
	}


}
