package com.leanxcale.highdataingestion;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.leanxcale.highdataingestion.reader.Loan;
import com.leanxcale.highdataingestion.reader.LoansReader;
import lombok.extern.slf4j.Slf4j;


/**
 * This test uses prepare statements to insert the data.
 */

@Slf4j
public class Try4_PreparedStatements extends Try_abstract {

	public void createTable() throws SQLException {

		String SQL_CREATE_TABLE_HASH =
				"CREATE TABLE " + TABLE_NAME + " (" +
						COLS +
						"PRIMARY KEY (client_id,loan_id)" +
						")" +
						" PARTITION BY HASH (client_id)" +
						" DISTRIBUTE BY HASH";

		utils.createTable(SQL_CREATE_TABLE_HASH);
	}

  // tag::ingest[]
	public void ingestData() {
		try (LoansReader csv = new LoansReader();
			 Connection connection = utils.connect();
			 PreparedStatement prepStmt = connection.prepareStatement(utils.SQL_INSERT_PREPARED))
		{
			Loan loan;
			while ((loan = csv.read()) != null) {
				setParams(prepStmt, loan);
				prepStmt.executeUpdate();
				connection.commit();
				stats.send(1);
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
// end::ingest[]

// tag::setParams[]
	private void setParams(PreparedStatement prepStmt, Loan loan) throws SQLException {
		Object[] args = utils.getParams(loan);
		for (int j = 0; j < args.length; j++) {
			prepStmt.setObject(j + 1, args[j]);
		}
	}
// end::setParams[]
}
