package com.leanxcale.highdataingestion;

import com.leanxcale.highdataingestion.avl.AVLTree;
import com.leanxcale.highdataingestion.avl.LoanAvlKey;
import com.leanxcale.highdataingestion.reader.Loan;
import com.leanxcale.highdataingestion.reader.LoansReader;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

import lombok.extern.slf4j.Slf4j;


@Slf4j
public class DBUtils {

	protected final String tblName;

	public final String SQL_INSERT_PREPARED;
	private final String SQL_INSERT;


	public DBUtils(String tblName, String cols) {
		this.tblName = tblName;

		String[] columns = cols.split(",");
		String colNames = Arrays.stream(columns)
				.map(token -> token.split("\\s", 2)[0])
				.collect(Collectors.joining(","));

		SQL_INSERT = "UPSERT INTO " + tblName + " (" + colNames + ") VALUES (placeholder)";
		SQL_INSERT_PREPARED = SQL_INSERT.replace("placeholder",
											"?, ".repeat(columns.length - 1) + "?");
	}

	protected Connection connect() throws SQLException {
		String lxUrl = "jdbc:leanxcale://localhost:14420/db";
		String lxUser = "lxadmin";
		String  lxPwd = "foobar";
		Connection conn = DriverManager.getConnection(lxUrl, lxUser, lxPwd);
		conn.setAutoCommit(false);
		return conn;
	}

	protected void cleanDatabase() throws SQLException {
		log.info("Cleaning database");
		try (Connection connection = connect();
			 Statement stmt = connection.createStatement())
		{
			stmt.executeUpdate("drop table if exists " + tblName);
			connection.commit();
		}
	}

	public void createTable(String sql) throws SQLException {;
		log.info("Creating tables at LeanXcale");
		try (Connection connection = connect();
			 Statement stmt = connection.createStatement())
		{
			stmt.executeUpdate(sql);
			stmt.executeUpdate("CREATE ONLINE AGGREGATE " + tblName + "_oa AS COUNT(*) cnt FROM " + tblName);
			connection.commit();
		}
	}

	public String getSplitPoints(LoansReader loansReader, int regions) {
		//Read a sampling of a random 1% of the file in order to get the split points
		log.info("Looking for split points");
		AVLTree<LoanAvlKey> avlTree = new AVLTree<>();

		// Read 1% of file to estimate split points
		try (LoansReader.Section section = loansReader.createSection(0, 100))
		{
			loansReader.setDuration(0);
			Loan loan;
			while ((loan = loansReader.read(section)) != null) {
				avlTree.insert(new LoanAvlKey(loan.getClientId(), loan.getId())); //build am AVL tree with the data
			}
		}
		//    avlTree.printSort();
		List<LoanAvlKey> splitKeys = avlTree.getSplitKeys(regions); //get the split points from the balanced tree

		StringJoiner splits = new StringJoiner(", ");
		for (LoanAvlKey key : splitKeys) {
			splits.add("(" + key.getClientId() + ", " + key.getLoanId() + ")");
		}
		return splits.toString();
	}

	public String getInsertSql(Loan loan) {
		Object[] params = getParams(loan);
		List<String> values = new ArrayList<>(params.length);

		for (Object param : params) {
			if (param instanceof String) {
				values.add("'" + param + "'");
			} else {
				values.add(param.toString());
			}
		}
		// Check'Last_pymnt_d' is null. i = 27
		if (values.get(27).equalsIgnoreCase("''")) {
			values.set(27, "null");
		}

		return SQL_INSERT.replace("placeholder", String.join(",", values));
	}

	public Object[] getParams(Loan loan) {
		return new Object[] {
				loan.getId(), loan.getClientId(), loan.getTs(), loan.getIssue_d(), loan.getLoan_status(),
				loan.getLoan_amnt(), loan.getTerm(), loan.getInt_rate(), loan.getInstallment(), loan.getGrade(),
				loan.getEmp_length(), loan.getHome_ownership(), loan.getAnnual_inc(), loan.getVerification_status(),
				loan.getMths_since_issue_d(), loan.getDesc(), loan.getPurpose(), loan.getTitle(), loan.getZip_code(),
				loan.getAddr_state(), loan.getDti(), loan.getMths_since_earliest_cr_line(), loan.getInq_last_6mths(),
				loan.getRevol_util(), loan.getOut_prncp(), loan.getTotal_pymnt(), loan.getTotal_rec_int(),
				loan.getLast_pymnt_d(), loan.getMths_since_last_pymnt_d(), loan.getMths_since_last_credit_pull_d(),
				loan.getTotal_rev_hi_lim()
		};
	}
}
