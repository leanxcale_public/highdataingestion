package com.leanxcale.highdataingestion;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.leanxcale.highdataingestion.reader.Loan;
import com.leanxcale.highdataingestion.reader.LoansReader;
import lombok.extern.slf4j.Slf4j;

/**
 * This test inserts full sql sentences opening the connection each insert and committing for each row.
 */

@Slf4j
// tag::Try1[]
public class Try1_NaiveApproach extends Try_abstract {
// end::Try1[]
// tag::create[]
// tag::H1[]
  public void createTable() throws SQLException {
// end::H1[]
    String SQL_CREATE_TABLE_HASH =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    COLS +
                    "PRIMARY KEY (client_id,loan_id)" +
                    ")" +
                    " PARTITION BY HASH (client_id)" +
                    " DISTRIBUTE BY HASH";

    utils.createTable(SQL_CREATE_TABLE_HASH);
  }
// end::create[]
// tag::H2[]
  public void ingestData() {
// end::H2[]
		try (LoansReader csv = new LoansReader())
		{
			Loan loan;
			while ((loan = csv.read()) != null) {
				String sql = utils.getInsertSql(loan);
				try (Connection connection = utils.connect();
					 Statement stmt = connection.createStatement())
				{
					stmt.executeUpdate(sql);
					connection.commit();
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
				stats.send(1);
			}
		}
	}
}
