﻿# **High Data Ingestion**

## Test script

Download _highdataingestion_ project and run *mvn clean package -DskipTests* to compile the executable jar. Alternatively, run *./gradlew shadowJar*.

To run the _hdi.sh_ script, the following jars must be in the same directory: _lxjdbcdriver.jar_ and _highdataingestion.jar_


